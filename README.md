# Example start script for Atlassian Stash

To install the script run

	cd /path
	cp stash /etc/init.d/
	chmod 755 /etc/init.d/stash
	chkconfig --add stash
	chkconfig stash on
	
To remove the script run

	chkconfig --del stash
	rm /etc/init.d/stash
	
Usage: 

	service stash {start|stop|status|restart|force-reload}
	

